import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { routerTransition } from '../router.animations';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import { AngularFireAuth } from 'angularfire2/auth';



@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  private formSubmitAttempt: boolean;
  correo: any;
  pass: any;
  id: any;
  status: any;
  enviar: boolean;
  mensaje: string;
    constructor(private formBuilder: FormBuilder, private http: HttpClient,
        private translate: TranslateService, public afAuth: AngularFireAuth,
        public router: Router
        ) {
            this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
            this.translate.setDefaultLang('en');
            const browserLang = this.translate.getBrowserLang();
            this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'en');
    }

    ngOnInit() {
      this.registerForm = this.formBuilder.group({
        correo: ['', [Validators.required,  Validators.email]],
        pass: ['', [Validators.required, Validators.minLength(8)]]
      });
      this.enviar = false;
      this.mensaje = 'Bien, Tus Datos Son Correctos';
    }

   /* onLoggedin() {
        localStorage.setItem('isLoggedin', 'true');
    }*/
  get f() { return this.registerForm.controls; }
  onLoggedin() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }/*
    this.afAuth.auth.signInWithEmailAndPassword(this.correo, this.pass).then(value => {
      console.log(this.mensaje);
      this.enviar = true;*/
      localStorage.setItem('isLoggedin', 'true');
      this.router.navigate(['/dashboard']);
   /* }).catch(err => {
      console.log('Ocurrio Algun Error:', err.message);
      alert('Ocurrio Algun Error, Verifica Tus Datos:' + err.message);
    });*/
    /*  localStorage.setItem('isLoggedin', 'true');
      this.router.navigate(['/dashboard']);*/
  }
}
