import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import {environments} from '../../environments/environment';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule} from '@angular/forms';

@NgModule({
    imports: [
        CommonModule, AngularFireModule.initializeApp(environments.firebase),
      ReactiveFormsModule, FormsModule,
        AngularFireAuthModule,
        TranslateModule,
        LoginRoutingModule],
    declarations: [LoginComponent]
})
export class LoginModule {}
