// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};
export const environments = {
  firebase: {
    apiKey: 'AIzaSyB9tizJE7s1K6WovGdll_mfg6mEcfcKSf0',
    authDomain: 'back-end-ditena.firebaseapp.com',
    databaseURL: 'https://back-end-ditena.firebaseio.com',
    projectId: 'back-end-ditena',
    storageBucket: 'back-end-ditena.appspot.com',
    messagingSenderId: '730451220490'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
